﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartNotes.Models
{
    public class NoteModel : BaseModel
    {

        public enum NoteDuration
        {
            ThirtySecond = 1,
            Sixteenth = 2,
            SixteenthDotted = 3,
            Eighth = 4,
            EighthDotted = 6,
            Fourth = 8,
            FourthDotted = 12,
            Half = 16,
            HalfDotted = 24,
            Whole = 32
        }

        public NoteDuration Duration { get; set; }
        public int PitchNum { get; set; }
    }
}
