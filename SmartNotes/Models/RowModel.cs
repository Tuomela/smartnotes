﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartNotes.Models
{
    public class RowModel : BaseModel
    {
        private ObservableCollection<MeasureModel> _measures;
        public ObservableCollection<MeasureModel> Measures
        {
            get
            {
                return _measures;
            }
            set
            {
                if (_measures != value)
                {
                    _measures = value;
                    NotifyPropertyChanged("Measures");
                }
            }
        }

    }
}
