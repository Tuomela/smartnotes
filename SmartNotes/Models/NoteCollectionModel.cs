﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartNotes.Models
{
    public class NoteCollectionModel : BaseModel
    {
        public ObservableCollection<NoteModel> Notes { get; set; }
        public NoteModel.NoteDuration Duration { get; private set; }

        public NoteCollectionModel(NoteModel.NoteDuration duration)
        {
            Notes = new ObservableCollection<NoteModel>();
            Duration = duration;
        }

        public NoteCollectionModel(int pitchNum, NoteModel.NoteDuration duration)
        {
            Notes = new ObservableCollection<NoteModel>();
            Notes.Add(new NoteModel()
            {
                PitchNum = pitchNum,
                Duration = duration
            });
            Duration = duration;
        }

        public void AddNote(NoteModel note)
        {
            if (note.Duration == Duration)
            {
                Notes.Add(note);
            }
        }
    }
}
