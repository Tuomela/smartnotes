﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartNotes.Models
{
    public class MeasureModel : BaseModel
    {
        private ObservableCollection<NoteCollectionModel> _chords;
        public ObservableCollection<NoteCollectionModel> Chords
        {
            get
            {
                if (_chords == null)
                {
                    _chords = new ObservableCollection<NoteCollectionModel>();
                }
                return _chords;
            }
            set
            {
                if (_chords != value)
                {
                    _chords = value;
                    NotifyPropertyChanged("Chords");
                }
            }
        }
        public NoteModel.NoteDuration BaseDuration { get; set; }
        public int BaseNumber { get; set; }

        public int GetUsedSteps()
        {
            int steps = 0;
            foreach (NoteCollectionModel note in Chords)
            {
                steps += (int)note.Duration;
            }
            return steps;
        }

        public void AddChord(NoteCollectionModel chord)
        {
            if (GetStepNum() - GetUsedSteps() >= (int)chord.Duration)
            {
                Chords.Add(chord);
            }
        }

        public void InsertChord(NoteCollectionModel chord, int index)
        {
            if (GetStepNum() - GetUsedSteps() >= (int)chord.Duration)
            {
                Chords.Insert(index, chord);
            }
        }

        public int GetStepNum()
        {
            return BaseNumber * (int)BaseDuration;
        }
    }
}
