﻿using SmartNotes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartNotes.Views
{


    /* 
     * -
     * -
     * -
     * -
     * -------------------
     * -------------------
     * -------------------
     * -------------------
     * -------------------
     * -
     * -
     * -
     * -
     */

    /// <summary>
    /// Interaction logic for MeasureControl.xaml
    /// </summary>
    public partial class MeasureControl : UserControl, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public static double PitchesNum = 32;
        public static double MeasureSpace = 10;
        public static double NoteStemLength = 50;

        public static NoteModel.NoteDuration CurrentNoteDuration = NoteModel.NoteDuration.Fourth;

        private MeasureModel _model;
        public MeasureModel Model
        {
            get
            {
                return _model;
            }
            set
            {
                if (_model != value)
                {
                    _model = value;
                    NotifyPropertyChanged("Model");

                    DrawLines();
                }
            }
        }

        public MeasureControl()
        {
            InitializeComponent();
            Height = PitchesNum * MeasureSpace;
        }

        public void DrawLines()
        {
            LinesCanvas.Children.Clear();
            for (int n = 10; n <= 18; n += 2)
            {
                Path path = new Path();
                path.Stroke = new SolidColorBrush(Colors.Black);
                LineGeometry line = new LineGeometry(new Point(0, n * MeasureSpace), new Point(ActualWidth, n * MeasureSpace));
                path.Data = line;

                LinesCanvas.Children.Add(path);
            }
        }

        public void CanvasLoaded(object sender, EventArgs args)
        {
            if (Model != null)
            {
                DrawLines();
            }
        }

        public double GetXPos(NoteCollectionModel chord)
        {
            int steps = 0;
            foreach (NoteCollectionModel n in Model.Chords)
            {
                if (n == chord)
                {
                    break;
                }
                steps += (int)chord.Duration;
            }
            return (ActualWidth / ((int)Model.BaseDuration * Model.BaseNumber)) * (steps+0.5*((int)(NoteModel.NoteDuration.Whole)/(int)chord.Duration));
        }

        private void MouseUp_event(object sender, MouseEventArgs e)
        {
            Point position = e.GetPosition(this);
            int measureNum = (int)Math.Round(position.Y / MeasureSpace);
            position.Y = measureNum * MeasureSpace;

            NoteCollectionModel note = new NoteCollectionModel(measureNum, CurrentNoteDuration);

            Model.AddChord(note);

            DrawNotes();
        }

        private void DrawNotes()
        {
            NoteCanvas.Children.Clear();
            foreach (NoteCollectionModel chord in Model.Chords)
            {
                foreach (NoteModel note in chord.Notes)
                {
                    DrawNote(note, GetXPos(chord));
                }
            }
        }

        private void DrawNote(NoteModel note, double XPos)
        {
            Path path = new Path();
            path.Stroke = new SolidColorBrush(Colors.Black);
            if ((int)note.Duration < (int)NoteModel.NoteDuration.Half)
            {
                path.Fill = new SolidColorBrush(Colors.Black);
            }

            GeometryGroup group = new GeometryGroup();

            EllipseGeometry ellipse = new EllipseGeometry(new Point(XPos, note.PitchNum * MeasureSpace), MeasureSpace, MeasureSpace);
            group.Children.Add(ellipse);

            if ((int)note.Duration < (int)NoteModel.NoteDuration.Whole)
            {
                int dir = note.PitchNum < 12 ? 1 : -1;
                LineGeometry line = new LineGeometry(new Point(XPos + MeasureSpace, note.PitchNum * MeasureSpace), new Point(XPos + MeasureSpace, note.PitchNum * MeasureSpace + NoteStemLength*dir));
                group.Children.Add(line);
            }

            path.Data = group;
            NoteCanvas.Children.Add(path);
        }

        private void LinesCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Model != null)
            {
                DrawLines();
            }
        }

        private void NoteCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Model != null)
            {
                DrawNotes();
            }
        }
    }
}
